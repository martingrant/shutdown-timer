# README #

Simple GUI for the Windows command prompt "shutdown" command which lets you set a time delay for a shutdown, e.g. set your computer to shut down 2 hours from the present. Set a time in days, hours and minutes and set a timer or cancel any scheduled shutdown. 

This was a really quick program thrown together for my own use as I often used the shutdown command when I left my computer running overnight to download games or run backups. 

### Todo ###

* Show the current scheduled shutdown on the GUI
* Investigate making it cross platform for macOS and Linux 
* Restructure/rewrite code as it is mostly the standard Visual Studio C# Winforms template
* Restructure the source folder, removing meta files and move the install .exe to more obvious place

### Contact ###
* @_martingrant
* martingrant@outlook.com
* www.martingrant.net