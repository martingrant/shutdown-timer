﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Shutdown Timer")]
[assembly: AssemblyDescription("Schedule a period of time for your computer to wait before shutting down. Useful if you have to leave your computer when uploading/downloading large files, or running a backup.                                                                                        Website: www.martingrant.net                                                                   Twitter: @_martingrant                                                                                             Email: martingrant@outlook.com")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Martin Grant")]
[assembly: AssemblyProduct("Shutdown Timer")]
[assembly: AssemblyCopyright("Copyright Martin Grant © 2017")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("ae789abd-8174-45b4-bea3-d74617b772ee")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.1.0.4")]
[assembly: AssemblyFileVersion("1.1.0.4")]
