﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

// Created by Martin Grant
// Last edited: 05/02/2017
// www.martingrant.net

namespace Shutdown_Timer
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int minutesInt = Int32.Parse(TextBoxMinutes.Text);
            int hoursInt = Int32.Parse(TextBoxHours.Text);
            int daysInt = Int32.Parse(TextBoxDays.Text);

            if (minutesInt > 0 || hoursInt > 0 || daysInt > 0)
            {
                ProcessStartInfo commandPromptSettings = new ProcessStartInfo();
                commandPromptSettings.FileName = @"C:\Windows\System32\cmd.exe";
                commandPromptSettings.RedirectStandardOutput = true;
                commandPromptSettings.RedirectStandardError = true;
                commandPromptSettings.RedirectStandardInput = true;
                commandPromptSettings.UseShellExecute = false;
                commandPromptSettings.CreateNoWindow = true;

                Process commandPrompt = new Process();
                commandPrompt.StartInfo = commandPromptSettings;

                commandPrompt.EnableRaisingEvents = true;
                commandPrompt.Start();
                commandPrompt.BeginOutputReadLine();
                commandPrompt.BeginErrorReadLine();
                
                int milliseconds = (daysInt * 86400) + (hoursInt * 3600) + (minutesInt * 60);

                string argument = "shutdown -s -t " + milliseconds.ToString();

                commandPrompt.StandardInput.WriteLine(argument);
                commandPrompt.StandardInput.WriteLine("exit");            
            }
            else
            {
                MessageBox.Show("Please select a time at least 1 minute or higher.");
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }



        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutBox1 aboutWindow = new AboutBox1();
            aboutWindow.Show();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            ProcessStartInfo commandPromptSettings = new ProcessStartInfo();
            commandPromptSettings.FileName = @"C:\Windows\System32\cmd.exe";
            commandPromptSettings.RedirectStandardOutput = true;
            commandPromptSettings.RedirectStandardError = true;
            commandPromptSettings.RedirectStandardInput = true;
            commandPromptSettings.UseShellExecute = false;
            commandPromptSettings.CreateNoWindow = true;

            Process commandPrompt = new Process();
            commandPrompt.StartInfo = commandPromptSettings;

            commandPrompt.EnableRaisingEvents = true;
            commandPrompt.Start();
            commandPrompt.BeginOutputReadLine();
            commandPrompt.BeginErrorReadLine();

            commandPrompt.StandardInput.WriteLine("shutdown -a");
            commandPrompt.StandardInput.WriteLine("exit");
        }

       
     
        private void label4_Click(object sender, EventArgs e)
        {
            
        }
    }
}
